from vedavaapi.common.api_common import get_current_org

from .. import VedavaapiSling


def myservice():
    return VedavaapiSling.instance

def sandhi_joiner():
    return myservice().sandhi_joiner(get_current_org())

from .v1 import api_blueprint_v1

blueprints_path_map = {
    api_blueprint_v1: '/v1'
}
