import subprocess
import sys
import re
import json
import os
from os.path import join
from functools import reduce #for 3.x
from indic_transliteration import sanscript

class Pada(object):
    ac_syms = 'aAiIuUfFxXeEoOMH'
    svara_syms = '\\\/!\^\~\#\%\$\&1-9'
    pada_pattern = '(.*?[{}]+|[^{}]+?)([{}]*)'.format(ac_syms, ac_syms, svara_syms)
    @staticmethod
    def split_varnas(arg1):
        split_varnas = re.findall(Pada.pada_pattern, arg1)
        return split_varnas

    def __init__(self, word_slp1):
        self.w = word_slp1
        self.analysis = list(map(lambda x: {'v' : x[0], 's' : x[1]},
            Pada.split_varnas(self.w)))
        self.varnas = reduce((lambda x, y: x + y), [w['v'] for w in self.analysis])
    def __str__(self):
        return self.w
    def __repr__(self):
        return json.dumps(self.to_dict())

    def to_dict(self):
        return {'pada' : self.w, 'varnas': self.varnas, 'analysis' : self.analysis }

    @classmethod
    def from_dict(cls, attrs):
        pada = cls(attrs['pada'])
        pada.w = attrs['pada']
        pada.varnas = attrs['varnas']
        pada.analysis = attrs['analysis']
        return pada

class PadaEncoder(json.JSONEncoder):
    def default(self, obj):
        if isinstance(obj, Pada):
            return obj.to_dict()
        # Let the base class default method raise the TypeError
        return json.JSONEncoder.default(self, obj)

# Format of sandhi_overrides sheet
# First header row has header fields Purva_pattern,Uttara_pattern,Samhita_pattern,Shaakha,Priority
# A row that starts with a '#' is ignored as comment.
# Within a purva_pattern, one can use a macro to
# refer to a list of items via __M:<macroname>__
# For example, to refer to chars 'a i u', embed __M:aR1__
#
# Within an uttara_pattern, one can embed a python expression to be
# evaluated via __<expr>__
#
class SandhiJoiner:

    ESSENTIAL_CONFIG_PARAMS = ['sclpath', 'sandhi_gsheet_id', 'varna_sandhi_rules_sheet', 'svara_sandhi_rules_sheet', 'macros_sheet']

    def __init__(self, config, gservices):
        self.validate_config(config)
        self.config = config
        self.gservices = gservices

        self.varna_sandhi_rules = []
        self.svara_sandhi_rules = []
        self.macros = {}

        self.sclpath = self.config['sclpath']
        self.reload()

    @classmethod
    def validate_config(cls, config):
        missed_config_params = [param for param in cls.ESSENTIAL_CONFIG_PARAMS if param not in config]
        if not len(missed_config_params):
            return
        else:
            raise KeyError("cannot instantiate SandhiJoiner. missing essential configuration parameters " + str(missed_config_params))


    def _get_table(self, sheetname, valformat='rows'):
        response_table , response_code = self.gservices.gsheets().sheet_values_for(
            self.config['sandhi_gsheet_id'], sheetname,
            pargs={'idType':'title', 'valuesFormat':valformat})

        if 'error' in response_table:
            print ("Error retrieving contents of spreadsheet {}: {}".format(sheetname, response_code))
            return None
        
        table = []
        rows_list = response_table.get('values', [])
        for row in rows_list:
            table.append(row)
        return table
        
    
    def reload(self):
        self.varna_sandhi_rules = self._get_table(self.config['varna_sandhi_rules_sheet'])
        if not self.varna_sandhi_rules:
            sys.exit(1)
        
        self.svara_sandhi_rules = self._get_table(self.config['svara_sandhi_rules_sheet'])
        if not self.svara_sandhi_rules:
            sys.exit(1)
        
        macros_table = self._get_table(self.config['macros_sheet'], valformat='maps')
        if not macros_table:
            sys.exit(1)

        for amap in macros_table:
            vals = amap.get('value', '').split()
            chars = reduce(lambda x, y: x and y, [len(x) > 1 for x in vals])
            vals = "|".join(vals) if chars else "[" + "".join(vals) + "]"
            self.macros[amap.get('macro')] = vals
        #print self.macros

    def _expand_macro(self, matchObj):
        [op, parms] = matchObj.group(1).split(':')
        if op == 'M':
            if parms in self.macros:
                return self.macros[parms]
        return ""

    def _compute_samhita(self, purva_pat, para_pat, samhita_pat, word1, word2):
        p1 = re.sub(r'__(.*?)__', self._expand_macro, purva_pat)
        m1 = re.match(p1, word1)
        if not m1:
            return None
        p2 = re.sub(r'__(.*?)__', self._expand_macro, para_pat)
        m2 = re.match(p2, word2)
        if not m2:
            return None
        matches = [s for s in m1.groups()]
        matches.extend([s for s in m2.groups()])
        #print matches

        def subst_args(matchobj):
            return matches[int(matchobj.group(1))-1]
        samhita = re.sub(r'\$(\d+)', subst_args, samhita_pat)

        def exec_expr(matchobj):
            return eval(matchobj.group(1))
        samhita = re.sub(r'__(.*?)__', exec_expr, samhita)
        return samhita

    # Input and output is in SLP1 format
    def apply_varna_sandhi(self, pada1, pada2, shakha=None):
        results = []
        #print pada1, pada2
        for p in self.varna_sandhi_rules:
            #print(p)
            if shakha and shakha not in p[3]:
                continue
            samhita = self._compute_samhita(p[0], p[1], p[2], 
                    pada1.varnas, pada2.varnas)
            if not samhita:
                continue

            esc_p = ['##' + v + '##' for v in p]
            res = {'praTamapadam' : pada1,
                   'dvitIyapadam' : pada2,
                   'saMhitapadam' : Pada(samhita),
                   'apavAdaH' : esc_p }
            results.append(res)

        return results

    def join2_svaras(self, res):
        results = []
        #print pada1, pada2
        #print(json.dumps(res, cls=PadaEncoder, indent=4, ensure_ascii=False, separators=(',', ': ')))
        pada1 = res['praTamapadam']
        pada2 = res['dvitIyapadam']
        samhita = res['saMhitapadam']
        # If input padas don't have svaras, return varna sandhi result
        if not (re.search('[{}]+'.format(Pada.svara_syms), pada1.w) or
                re.search('[{}]+'.format(Pada.svara_syms), pada2.w)):
            return res
        # Input padas have svaras
        for p in self.svara_sandhi_rules:
            #print(p)
            # Skip rules not matching the Shakha
            if shakha and shakha not in p[0]:
                continue
            # Skip rules not matching the varNa sandhi
            if p[2] not in res['sanDiH']:
                continue

            # if svara sandhi rule description has any varNas,
            if (re.search('[a-zA-Z]+'.format(Pada.svara_syms), p[5]) or
                    re.search('[a-zA-Z]+'.format(Pada.svara_syms), p[6])):
                #   apply only the svara sandhi rule.
                p1 = re.sub(r'__(.*?)__', self._expand_macro, p[5] + '$')
                m1 = re.search(p1, pada1.w)
                if not m1:
                    continue
                p2 = re.sub(r'__(.*?)__', self._expand_macro, "^" + p[6])
                m2 = re.search(p2, pada2.w)
                if not m2:
                    continue
                purva = re.sub(p1, '', pada1.w)
                uttara = re.sub(p2, '', pada2.w)
                matches = [s for s in m1.groups()]
                matches.extend([s for s in m2.groups()])
                #print matches

                def subst_args(matchobj):
                    return matches[int(matchobj.group(1))-1]
                middle = re.sub(r'\$(\d+)', subst_args, p[7])
                #print purva, middle, uttara

                def exec_expr(matchobj):
                    return eval(matchobj.group(1))
                middle = re.sub(r'__(.*?)__', exec_expr, samhita)

                samhita = Pada(purva + middle + uttara)
            else:
                # Else 
                #   apply it on the result of varna sandhi. 

                # Look for the svara combination
                if (pada1.analysis[-1]['s'] != p[5]) or \
                        (pada2.analysis[0]['s'] != p[6]):
                    continue
                #print str(pada1.analysis[-1]) + " " + p[5]
                #print str(pada2.analysis[0]) + " " + p[6]
                # Identify the matching prefix of pUrva against samhita
                i = 0
                while i < range(len(pada1.analysis)):
                    if pada1.analysis[i]['v'] != samhita.analysis[i]['v']:
                        break
                    samhita.analysis[i]['s'] = pada1.analysis[i]['s']
                    i = i + 1
                # Identify the matching suffix of Uttara against samhita
                k = len(pada2.analysis)-1
                j = len(samhita.analysis)-1
                while k >= 0:
                    if pada2.analysis[k]['v'] != samhita.analysis[j]['v']:
                        break
                    samhita.analysis[j]['s'] = pada2.analysis[k]['s']
                    j = j - 1
                    k = k - 1
                #print samhita.analysis
                #print i, j
                if i <= j:
                    samhita_varnas = p[7].split()
                    k = 0
                    while i <= j:
                        samhita.analysis[i]['s'] = samhita_varnas[k]
                        i = i + 1
                        k = k + 1
                samhita_str = reduce((lambda x, y: x + y), [w['v']+w['s'] for w in samhita.analysis])
                samhita = Pada(samhita_str)

            esc_p = ['##' + v + '##' for v in p]
            res_out = res.copy()
            res_out['saMhitapadam'] = samhita
            res_out['svara-apavAdaH'] = esc_p
            results.append(res_out)
        return results

    # Given two words in SLP1 format, produce their samhita form
    # along with the sandhi detected and the sutras and prakriya used.
    def join2(self, pada1, pada2, shakha=None):
        results = self.apply_varna_sandhi(pada1, pada2, shakha)
        if len(results) == 0:
            args = ["perl", join(self.sclpath, "sandhi/mysandhi.pl")]
            args.extend(["WX", "any", 
                sanscript.transliterate(pada1.varnas, sanscript.SLP1, sanscript.WX), 
                sanscript.transliterate(pada2.varnas, sanscript.SLP1, sanscript.WX), 
                ])
            result = subprocess.check_output(args).decode('utf-8')
            #print(result)
            output = [sanscript.transliterate(re.sub('^:', '', val), \
                sanscript.WX, sanscript.SLP1) for val in result.split(",")]
            res=dict(zip(output[5:],output[0:5]))
            if not 'saMhitapadam' in res:
                return res
            res['praTamapadam'] = pada1
            res['dvitIyapadam'] = pada2
            res['saMhitapadam'] = Pada(res['saMhitapadam'])
            results = [res]

        return reduce((lambda x, y: x+y), 
            [self.join2_svaras(r) for r in results])

    # Given a list of words/morphemes, this function will join them according to
    # sandhi rules and give the final samhita word.
    def join(self, words, encoding, shakha=None, exceptions=None):

        def _join(samhita, words):
            if len(words) > 0:
                w = sanscript.transliterate(words[0], encoding, sanscript.SLP1)
                results2 = [self.join2(samhita, Pada(w), shakha)] #TODO it is getting dict instead of list of dicts. have to investigate. wrapped in list as temporary fix.
                for res in results2:
                    if not 'saMhitapadam' in res:
                        continue
                    mysamhita = res['saMhitapadam']
                    if len(words) > 1:
                        for r in _join(mysamhita, words[1:]):
                            yield {'samhita': str(r['samhita']), \
                                'analysis' : [res] + r['analysis']}
                    else:
                        yield { 'samhita' : str(mysamhita), 'analysis' : res }

        samhita = Pada(sanscript.transliterate(str(words[0]), encoding, sanscript.SLP1))
        results = [r for r in _join(samhita, words[1:])]
                
        results_str = sanscript.transliterate(
            json.dumps(results, cls=PadaEncoder), sanscript.SLP1, encoding)
        results = json.loads(results_str)

        return {'words' : words, 'results' : results }

def usage():
    print("Usage: {} <encoding> <shaakha_name>".format(sys.argv[0]))
    sys.exit(1)

if __name__ == "__main__":
    if len(sys.argv) < 2:
        print("Error: Not enough arguments.")
        usage()

    encoding = sys.argv[1].upper()
    shakha = None
    if len(sys.argv) >= 3:
        shakha = sys.argv[2].upper()
    encoding = eval('sanscript.' + encoding)

    console_input = raw_input if sys.version_info < (3, 0) else input
    from vedavaapi.google_helper import GServices
    test_creds_path = str(console_input('path to test credentials (return for default {}): '.format(join(os.path.dirname(__file__),"vedavaapi-credentials.json")))) or join(os.path.dirname(__file__),"vedavaapi-credentials.json")
    test_sclpath = str(console_input('path to test scl (return for default /home/sai/scl-dev/build): ')) or '/home/sai/scl-dev/build'
    print(test_creds_path, test_sclpath)

    test_gservices = GServices.from_creds_file(
        creds_path=test_creds_path,
        scopes=['https://www.googleapis.com/auth/drive.readonly', 'https://www.googleapis.com/auth/spreadsheets.readonly'])
    sandhi = SandhiJoiner({
        "sclpath": test_sclpath,
        "sandhi_gsheet_id": "1yP82Y5d5mGrvNV2e-OPc6rBhPEtTYWI2vEwYN7uZdnU",
        "varna_sandhi_rules_sheet": "varna_sandhi_rules",
        "svara_sandhi_rules_sheet": "svara_sandhi_rules",
        "macros_sheet": "Macros"
    }, test_gservices)

    while (1):
        try:
            line = str(console_input('Words to join: '))
        except:
            break
        clusters = line.rstrip("\n").split(',')
        for wcluster in line.rstrip("\n").split(','):
            padas = [w for w in wcluster.split()]
            res = sandhi.join(padas, encoding, shakha)
            print(json.dumps(res, indent=4, ensure_ascii=False, separators=(',', ': ')))

__all__ = ['SandhiJoiner']
