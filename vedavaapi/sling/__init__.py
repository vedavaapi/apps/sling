import logging

logging.basicConfig(
  level=logging.INFO,
  format="%(levelname)s: %(asctime)s {%(filename)s:%(lineno)d}: %(message)s "
)

# Dummy usage.
#logging.debug("So that depickling works well, we imported: " + str([common, ullekhanam, books, users]))

from vedavaapi.common import OrgHandler, VedavaapiService
from .sandhi_join import SandhiJoiner


class SlingOrgHandler(OrgHandler):

    def __init__(self, service, org_name):
        super(SlingOrgHandler, self).__init__(service, org_name)
        self.sandhi_joiner = SandhiJoiner(self.service.config, self.service.registry.lookup('gservices').services(self.org_name))


class VedavaapiSling(VedavaapiService):

    instance = None

    dependency_services = ['gservices']
    org_handler_class = SlingOrgHandler

    title = 'Vedavaapi Sling'
    description = 'vedavaapi samskritam linguistic tools'

    def __init__(self, registry, name, conf):
        super(VedavaapiSling, self).__init__(registry, name, conf)

    def sandhi_joiner(self, org_name):
        return self.get_org(org_name).sandhi_joiner
